| Tâche saisie | Lien |
| ------ | ------ |
| Espace perso et groupes : ajouter un tooltip à la loupe | [Lien](https://latelier.mim-libre.fr/projects/aYDaMDBPADAisCQFE/pRLnWyMYYZg8nxyWZ) |
| Espace perso : assombrir "Glisser les éléments ici" | [Lien](https://latelier.mim-libre.fr/projects/aYDaMDBPADAisCQFE/KxNKtY4pESn5bxZyB) |
| Assombrir les textes écrits en orange sur fond blanc (groupes modérés/restreints) | [Lien](https://latelier.mim-libre.fr/projects/aYDaMDBPADAisCQFE/dai9bcGfFYvxh4T4d) |
| Modifier le titre html de la page en fonction des pages affichées | [Lien](https://latelier.mim-libre.fr/projects/aYDaMDBPADAisCQFE/m7CexwS6cBCRwK9sC) |
| Menu principal : Rendre plus visible le focus dans les éléments de menu | [Lien](https://latelier.mim-libre.fr/projects/aYDaMDBPADAisCQFE/uEPrmEvo7RRSPKqwX) |
| Mettre en avant le focus sur les boutons du footer | [Lien](https://latelier.mim-libre.fr/projects/aYDaMDBPADAisCQFE/RpgDWvdxzLwnrWQMJ) |
| Sur la page d'accueil: Renforcer le contraste/couleur du texte indiquant le numéro de version du site. | [Lien](https://latelier.mim-libre.fr/projects/aYDaMDBPADAisCQFE/StmZMTYBXXZ9NWmjS) |
| Navigation générale : ajouter des liens d'évitement | [Lien](https://latelier.mim-libre.fr/projects/aYDaMDBPADAisCQFE/C8nYmNufw7WKFjWKp) |
|  | [Lien]() |