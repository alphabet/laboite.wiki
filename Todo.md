Liste des fonctionnalités à implémenter
=======================================

tests manquants (publications)
------------------------------

    *  appsettings.introduction
    *  appsettings.gcu
    *  appsettings.legal
    *  appsettings.accessibility
    *  appsettings.personalData
    *  articles.one
    *  groups.adminof --> OK
    *  groups.one.admin --> OK
    *  groups.users --> OK
    *  services.one.admin --> OK
    *  services.group --> OK
    *  services.one --> OK
    *  roles.admin --> OK
    *  users.group --> OK
    *  users.publishers --> OK
    *  users.admin --> OK

tests manquants (méthodes)
--------------------------

    *  appSettings.updateAppsettings
    *  appSettings.updateIntroductionLanguage
    *  appSettings.getAppSettingsLinks
    *  articles.visitArticle
    *  articles.downloadBackupPublications
    *  articles.uploadBackupPublications
    *  articles.checkSelectedInPublications
    *  languages.getAll --> OK
    *  personalspaces.checkPersonalSpace --> OK
    *  get_users.group_count --> méthode calculant les entrées total pour la publication paginée users.group
    *  get_users.publishers_count --> idem pour users.publishers (nécéssite un refactoring en ValidatedMethod si on veut tester)
    *  get_groups.all_count --> (idem)
    *  setAnimatorOf --> testé dans groups.tests.js
    *  unsetAnimatorOf --> testé dans groups.tests.js
    *  setMemberOf --> testé dans groups.tests.js
    *  unsetMemberOf --> testé dans groups.tests.js
    *  setCandidateOf --> testé dans groups.tests.js
    *  unsetCandidateOf --> testé dans groups.tests.js
    *  findUser --> OK
    *  setKeycloakId --> OK

autres fonctions
----------------

    *  addNotification (api REST : notifications/server/rest.js)
    *  tests sur les clients Nextcloud/Keycloak ?
    *  tests sur les appels s3 (files.upload, files.removeFolder, files.selectedRemove, files.move, files.user)

    Ces tests nécessiteraient de simuler le retour de chaque appel externe.

    Cela semble faisable avec la notion de sandbox de la librairie sinon. cf https://medium.com/swlh/a-quick-demo-how-to-write-unit-test-calling-thirdparty-service-with-mock-in-node-js-7dfdb755b02a

code mort (non utilisé)
-----------------------

    *  méthode groups.findGroups (remplacée par une publication avec pagination) --> SUPPRIME
    *  categories.service --> SUPPRIME (categories déjà incluses dans service.one)
    *  users.fromlist --> SUPPRIME
    *  groups.memberof --> SUPPRIME