# Démontrer ce qu'est Rizomo et ce qu'il est possible de faire avec

## Objectif de cette page

* Disposer d'un fil conducteur pour une démonstration complète
  * sur le bon environnement
  * en passant sur tous les points qui sont important
* Disposer d'un fil conducteur pour faire un test de non régression
  * avoir un parcours (ou plusieurs) permettant d'utiliser l'ensemble des fonctions nécessaire pour Rizomo
  * si possible disposer d'un chapitre pour les fonctions en attente, avec les tests de recette

## Environnement de test

/!\\ Attention: Chaque environnement à sa propre base de donné, il faut avoir un compte sur chacune d'elle.

[Env. de démo par défaut](https://rizomo-demo.numerique.gouv.fr) :

* il est sensé être stable,
* il est basé sur la branche rizomo-demo (développements testés/validés pour le projet + la branche spécifique au projet + les branches en attente de validation EOLE)
* il contient les développements rizomo testés/validés non encore intégré dans Dev

[Env. de dev de l'application](https://lb.appseducation.fr/)

* c'est la branche qui servira comme base pour la testing puis master
* elle ne contient que des développements testés/validés pour le projet
* elle n'intègre que les développements testés/validés qui sont dans le milestone courant

## tests d'usage

Vérification en // avec le document https://codimd.mim-libre.fr/s5wwoDZYT2iC9u4tAjGARg?edit pour avoir la même structure quand c'est possible

### Se créer un compte dans "rizomo"

Tous les comptes email en test-rizomo sont des redirections vers le mail dinum@heintz.ovh, s'il y a besoin de lire les mails aller sur le webmail ovh et demander le mot de passe à Olivier Heintz. Les comptes dispo pour les tests sont:

* test-rizomo@dinum.heintz.ovh
* test-rizomo@hors-etat.heintz.ovh
* test-rizomo@mim-transfo.heintz.ovh
* test-rizomo@psn.heintz.ovh
* test-admin@dinum.heintz.ovh

Attention, lors de l'enregistrement des comptes via agent connect, il ne faut pas mettre de point "." dans le nom de famille, agentConnect ne le supporte pas.
A la fin des tests, merci de penser à ne pas les laisser actif, les détruire dans l'env. rizomo et le ou les keycloack associés

**_Cas d'usage_** :

* création d'un compte directement dans rizomo avec l'email test-rizomo@dinum.heintz.ovh
  * il doit être affecté directement à la structure dinum
* création d'un compte directement dans rizomo avec l'email test-rizomo@hors-etat.heintz.ovh
  * création impossible