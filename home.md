[Conception de l'application](Conception)

[Fonctionnalités à implémenter](Todo)

[Système de téléversement](Syst%C3%A8me-de-t%C3%A9l%C3%A9versement)

[Gestion du SSO avec Keycloak](Gestion-du-SSO-avec-Keycloak)

[Groupes et permissions dans Keycloak](Intégration-des-groupes-et-permissions-dans-Keycloak)

[Intégration de BigBlueButton pour les groupes](BigBlueButton)

[[Obsolète] Création d'une release (git)](Cr%C3%A9ation-d'une-release-(git))

[[Obsolète] Branches de Hotfix](Branches-de-Hotfix)