| **Attention doc obsolète depuis l'arrivée du CI qui gère tout automatiquement** |
| ------------------------------------------------------------------------------- |

<details><summary>Voir quand même</summary>
Les releases sont des publications stabilisées et versionnées du projet dans la branche master. Voici la procédure pour publier une release dans master à partir de la branche dev ([référence](https://nvie.com/posts/a-successful-git-branching-model/#release-branches)).



0. Avant de commencer, s'assurer que la branche dev est dans un état stable : toutes les fonctionnalités voulues ont été intégrées et testées, aucun développement n'est en cours.

1. Définir le numéro de version de cette release en fonction des nouvelles fonctionnalités apportées ([référence](https://semver.org/lang/fr/)).

Étant donné un numéro de version MAJEUR.MINEUR.CORRECTIF, il faut incrémenter :

    le numéro de version MAJEUR quand il y a des changements non rétrocompatibles,
    le numéro de version MINEUR quand il y a des ajouts de fonctionnalités rétrocompatibles,
    le numéro de version de CORRECTIF quand il y a des corrections d’anomalies rétrocompatibles.

Des libellés supplémentaires peuvent être ajoutés pour les versions de pré-livraison et pour des méta-données de construction sous forme d’extension du format MAJEURE.MINEURE.CORRECTIF.

2. Créer la branche de release à partir de dev (NUM_VER = numéro de version voulue) :
```
NUM_VER="3.0.0"
echo "Numéro de la release : $NUM_VER"
git checkout -b release-$NUM_VER dev
```
3. Effectuer tous les changements relatifs à cette nouvelle version (package.json, changelog, aides...)

4. Commiter ces changements dans la branche de release :
```
git commit -a -m "Update version number to $NUM_VER"
```
5. Merger cette branche de release dans master :
```
git checkout master
git merge --no-ff release-$NUM_VER
git push
```
6. Tagger la branche master :
```
git tag -a $NUM_VER -m "Release $NUM_VER"
git push origin $NUM_VER
```
7. Répercuter les changements dans la branche dev :
```
git checkout dev
git merge --no-ff release-$NUM_VER
git push
```
8. Supprimer la branche de release :
```
git branch -d release-$NUM_VER
```
Si des bugs sont trouvés dans la branche master, il doivent être traités dans une [branche de hotfix](Branches-de-Hotfix)
</details>