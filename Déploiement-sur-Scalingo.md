Il existe plusieurs manières de déployer sur scalingo

### Intégration SCM Scalingo

L'intégration SCM est un lien entre une branche du repo du projet et l'application Scalingo concernée. Cela peut se faire via leur interface ou leur CLI avec la commande suivante

`scalingo --app rizomo-prod --region osc-secnum-fr1 integration-link-create --auto-deploy --branch rizomo-prod https://gitlab.mim-libre.fr/alphabet/laboite`

Cette action permet a l'application de se mettre à jour automatiquement à chaque modification de la branche correspondante

\
Pour réussir, il faut cependant avoir le role de **Maintainer** sur le projet GitLab

#### Git push

Un déploiement peut se faire aussi par simple git push sur le repo git. il faut par contre configurer une clé SSH dans votre compte scalingo pour avoir l'accès

#### Déploiement manuel avec Scalingo CLI

Pour provoquer un déploiement manuel sur un repo deja lier, la commande suivante est à faire

`scalingo --app rizomo-integ integration-link-manual-deploy rizomo-integ
`