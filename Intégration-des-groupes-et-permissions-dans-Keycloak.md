Le but serait de transmettre les informations de groupes dans les applications (/le statut d'admin global ?).

Keycloak propose plusieurs fonctionnalités qui pourraient servir dans ce but :

*  Groupes définis au niveau du realm. Les utilisateurs membres d'un groupe héritent des roles et attributs définis sur ce groupe. Les rôles sont définis soit au niveau du realm, soit au niveau du client. Il est possible de créer des rôles composites. Le scope roles est automatiquement transmis à la connexion (openid connect). Les rôles sont rattaché à l'utilisateur, et non au groupe, il faudrait à priori créer plusieurs groupe et rôles par groupe créé dans laboite: 1 groupe d'admins, 1 groupe de membres et les rôles associés rattachés à chaque groupe (member_mongroupe, admin_mongroupe). L'application cliente pourrait ensuite se baser sur le libellé du rôle pour déterminer le groupe/rôle concernés. 
 
* Service d'autorisation (resource server): https://www.keycloak.org/docs/latest/authorization_services/
  Permettrait de définir des ressources de type groupe, et d'assigner des permissions à l'utilisateur sur celles-ci. Nécéssite de requêter le serveur d'autorisation depuis l'application.


**Solution en cours de développement :**

* à l'initialisation du serveur, un groupe 'admins' est créé si non existant
* lorsqu'un groupe 'toto' est créé dans la boite, on crée automatiquement les groupes suivants dans Keycloak : 'member_toto', 'animator_toto' et 'admin_toto' (realm) ainsi que des rôles du même nom (dans le client laboite)
* lorsqu'un rôle sur un groupe est ajouté/supprimé dans laboite, on ajoute/enlève le groupe correspondant dans Keycloak
* Rqe: en modifiant le tokenClaimName des mappers du scope 'roles' dans Keycloak, on peut controller le nom de l'attribut qui recevra les roles de l'utilisateur à la connexion.

**Pour tester :**

Création d'un serveur Keycloak de test:

* docker run -p 8080:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin quay.io/keycloak/keycloak:latest (attention, la conf est perdue en cas d'arrêt)
* aller sur l'[interface d'admin](http://localhost:8080/auth/admin) : admin/admin
* survoler le nom de realm (master en haut à gauche) et cliquer sur "Add realm"
* importer ce fichier pour créer un realm "local" et un client "laboite" : [realm-export_7_.json](uploads/e925f7588e9a6c52e4f973bcc6774632/realm-export_7_.json)
* récupérer la clé publique du realm : [http://localhost:8080/auth/realms/local/](http://localhost:8080/auth/realms/local/)
* renseigner les settings de laboite : "enableKeycloak": true, "keycloakUrl": "http://localhost:8080/auth", "keycloakRealm": "local" et "pubkey": "...", "client": "laboite", "adminUser": "admin", "adminPassword": "admin"
* se connecter à laboite et enregistrer un nouvel utilisateur (attention, ne pas utiliser un email déjà présent dans laboite)

Tests

vérifier dans les logs de laboite que le client s'initialise bien (au premier lancement, un message doit indiquer la création du groupe admins dans Keycloak):

`
I20200630-16:04:21.656(2)? Keycloak: client ID found (4777b0f9-a749-40e9-9a89-d00a65cb4ee3)
I20200630-16:04:21.696(2)? Keycloak: group admins added (id d9d58c0b-5ea9-4692-8766-bd73a7f44100)
I20200630-16:04:21.718(2)? Keycloak: API client initialized
`

Se connecter à laboite avec un compte keycloak
Créer un nouveau groupe
s'assigner comme menbre du groupe

Dans l'interface admin de keycloak, vérifier que :
* 3 groupes ont été créés pour le groupe en question (member_\<group\>, animator_\<group\>, admin_\<group\>)
* 3 rôles correspondants ont été créés dans le client laboite (ou autre nom, vérifier le nom du client configuré dans les settings)
* éditer l'utilisateur en question, aller sur "Role Mappings" et entrer le nom du client dans "Client Roles". Dans "effective roles", l'utilisateur devrait avoir admin_\<group\> et member_\<group\>.

**Fonctionnement revu**

* on crée seulement un groupe avec le même nom que dans laboite (plus de groupes et roles admin_\<groupe\>, member_\<groupe\>, animator_\<groupe\>)
* on inscrit dans ce groupe les membres et animateurs (la notion d'admin de groupe devient une notion exclusivement interne à laboite)
* keycloak doit être paramêtré pour renvoyer la liste des groupes de l'utilisateur dans le jeton.

Les applications recevront donc la liste des groupes dont l'utilisateur est membre ou animateur (sans distinction de role).