Le téléversement de La Boite est segmenté en plusieurs fonctionnalités et fichiers de manière à avoir un système global et centralisé facilement réutilisable dans tous les composants/pages afin d'éviter de recommencerun upload system a à chaque fois qu'il y en a besoin d'un.

## 1. Configuration

Dans `api/files/server` il y a le fichier de config dans lequel le client minio est créé avec la librairie. Ce client est exporté pour être utilisé par les méthodes a chaque action d'upload, de déplacemnt ou de suppression de fichier.


## 2. Les méthodes

*  **files.upload**: Cette méthode upload le fichier vers minio, il lui faut le fichier en base64, le nom comprenant l'extension et le chemin de dossier dans minio. 
*  **files.removeFolder**: Permet de supprimer les dossier. Dans Minio, les dossiers sont relatifs et ne sont pas des entités à part entières. un fichier aura pour nom '/dossier/dossier/fichier.ext'. Pour supprimer un dossier, il faudra donc lister donc les fichier qu'il contient et les supprimer un par un.
*  **files.selectedRemove**: ressemblant à la méthode précédente, elle prend comme argument, soit un tableau de fichiers à garder, soit un tableau de fichier a supprimer dans un dossier précis.
*  **files.move**: Cette methode copie tous les fichier d'une source a une destination et, après chaque copie, supprime les fichiers de la source.
*  **files.user**: Recupère tout simplement les fichier contenu dans le dossier d'un utilisateur a partir de sont ID


## 3. Le contexte de l'application

Le meilleur moyen de construire un système global dans l'application était d'utiliser le contexte de React. Avec la commande `dispatch` retournée par le contexte, il est simple d'ajouter de n'importe quelle page ou composant un fichier a la liste des uploads. Récupérer ensuite cette liste ailleurs grâce à ce même contexte est assez simple.
Exemple:
```javascript
dispatch({
        type: 'uploads.add',
        data: {
          name: "mon-fichier-renommé.png",
          fileName: "image.png",
          file: "data:image/png;base64,iVBORw0KGgoAAA.........",
          path: `users/${Meteor.userId()}`, // pour uploader dans le dossier de l'utilisateur par exemple
          storage: true, // pour savoir si on utilise la limite de stockage utilisateur ou pas
          onFinish: (url) => console.log(url), // une fonction qui recoit l'url a la fin de l'upload
        },
      });
```


## 4. Le composant UploaderNotifier

Ce composant est ajouté à la base de l'application, il est donc présent partout. Il recupère la liste des fichier à téléverser depuis le contexte React. Si un fichier est présent dans la liste, un toast de notification va s'afficher sur l'écran et montrer la fichier en cours de téléchargement. C'est aussi a cet endroit que s'afficheront les erreurs de téléversement après verification du fichier. 

À la création d'un toast de notification, le fichier va être testé et ensuite téléversé. Le fichier fileProcess contient les methodes à appliquer sur la chaine de caractère en base64 avant d'appeler la méthode Meteor pour envoyer le fichier au serveur Minio. Appeler une méthode Meteor pour exécuter cette tâche présente deux inconvénients:
*  Il est impossible de connaître la progression du téléversement
*  Le téléversement à lieu 2 fois, du client au serveur de l'appli, puis du serveur de l'appli au serveur Minio.
Il serait possible de faire différemment en essayant de copier un composant Meter fait pour S3 codé par [CulturalMe](https://github.com/CulturalMe/meteor-slingshot)

Une fois le téléversement réussi ou echoué, le toast de notification est supprimé après 5 secondes et l'upload est supprié de la liste du contexte avec un autre appel a la focntion dispatch.

## 5. Le composant ImageAdminUploader

Ce composant sert a afficher une image avec une taille que l'on donne par ses props. Cette image comprend un input de type file et une fonction pour ajouter un fichier à upoader au contexte. Ce composant n'est utilisable que dans une fonctionnalité admin car il ne comprend pas l'option `storage`


## 6. TO DO
- [ ] Trouver comment upload les fichier depuis le client vers Minio tout en ayant la validation serveur avant de facon a n'avoir qu'un seul upload
- [ ] Ajouter une pincée de poudre de magie sur le code
