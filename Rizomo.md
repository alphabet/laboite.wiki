# Liens utiles pour le suivi de Rizomo

* [Point Dév Rizomo](https://codimd.mim-libre.fr/FSTAJVX9SC242idbdZPwXQ)
* [Backlog Rizomo](https://codimd.mim-libre.fr/X7rY_TqHQ_iT1mgEJsoJdw)
* [Analyse Risque Rizomo](https://codimd.mim-libre.fr/1Th0Cm8lSNqCKmbtQ7xV0g?view)
* [Synchro Architecture Rizomo-LaBoite](https://codimd.mim-libre.fr/KdP72CUCRoKxrobgjGeAjg)

# Procédure de contribution pour le projet Rizomo (au sein de LaBoite)

1. Création d'une issue avec le label rizomo
   1. positionner un label d'avancement, puis le faire évoluer via édition de la tâche ou via le board.
      * Suggestion : si la tâche n'est pas dans le sprint courant
      * Todo : si la tâche doit être réalisé dans le sprint courant
      * In Progress : si la mergeRequest est créé et donc la branche
      * to review : le dev est fini, la discussion se continu au sein de la mergeRequest associée. Il doit y avoir une MergeRequest quand le tag est à "to review"
   2. s'il y a discussion sur le périmètre de la tâche, plutôt corriger/développer la description de la tâche que d'ajouter des commentaires. C'est la description qui sera utilisé pour la review de la MergeRequest associé.
2. le libellé de la branche associée doit contenir le N° de l'issue et son titre
3. la MergeRequest associé doit être en draft tant que le dev. n'est pas ok et qu'il peut y avoir des commits
4. Validation de la MergeRequest (quand le dev est fini):
   1. si possible trouver un second dev pour approuver
   2. passage en ready et affectation de quelqu'un de l'équipe PO-Rizomo en review
   3. suite à la review, s'il y a des corrections / évolutions, mettre un commentaire et repasser en draft
   4. si pour le besoin de l'intégration de cette branche sur un environnement de test (INTEG ou autre), il faut faire un rebase, pas de soucis tant que les reviewer ne sont que de l'équipe Rizomo. Les seul commit autorisé sur les mergeRequests en Ready sont ceux lié à la résolution de conflit pour l'intégration sur un environnement de test (INTEG ou autre)
   5. la fin de la review doit avoir lieu sur INTEG, lors du merge sur INTEG, la noter dans la liste des branches à merger (codiMD) \
      si c'est OK, changer le reviewer pour mettre Luc, plus aucune commit n'est autorisé tant que la MR est en ready
   6. Suite aux remarques de Luc ou son équipe, les corrections/améliorations ne sont ajouté que à partir du moment où la MR est repassé en draft.
      1. s'il y a besoin de review PO-Rizomo changer le reviewer et retour au point 3.
5. Re-création de la branche INTEG et rebase nécessaire pour une MR
   * si la MR est en review rizomo, rebase de la branche
   * si la MR est en review MENJS, rebase uniquement en local, donc integration des commits dans Integ.

# Cycle d'évolution des branches rizomo

### rizomo-specific :

Elle ne doit comporter que très peu de commit, spécifique à rizomo (non intégrable dans LaBoite)

Chaque commit doit être explicite sur le plan fonctionnel, cela ne peut être que des ajouts ou changements jamais une corrections qui font forcément l'objet d'une issue et d'une MR.

Avant chaque repositionnement de la branche rizomo-integ, il faut faire un rebase sur dev et au besoin regrouper des commits pour rester clair sur le rôle de chaque commit.

### rizomo-integ :

Elle est repositionné sur la branche dev après chaque Sprint-demo, puis sont appliqués toutes les branches "rizomo" en attente de merge / à tester