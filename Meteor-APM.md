# What is it ?
Meteor APM (Application Performance Monitoring), originally known as Kadira, is a performance monitoring and management tool specifically designed for Meteor applications. Meteor is a full-stack JavaScript platform that enables developers to build real-time web and mobile applications.

Kadira was the first APM solution for Meteor, but the company behind it ceased operations in 2017. The open-source community took over the project, and Meteor APM was born. Later, Monti APM emerged as a successor to Meteor APM, offering a more feature-rich, maintained, and supported version of the tool.

These tools provide several key features for monitoring and managing the performance of Meteor applications:

    1. Real-time performance metrics: it gives you an overview of your application's performance, including response times, throughput, and errors, allowing you to quickly identify and resolve issues.
    2. Error tracking: it helps track and analyze errors occurring in your application, making it easier to find the root cause and fix issues.
    3. Method and publication tracing: it allows you to monitor and analyze how your application's methods and publications are performing, identifying slow or problematic parts of your code.
    4. CPU and memory profiling: it offers insights into your application's CPU and memory usage, helping you optimize resource consumption and prevent bottlenecks.
    5. Alerts and notifications: it provides customizable alerts and notifications to help you stay informed about your application's performance and potential issues.

Monti APM, as the successor to Meteor APM, continues to improve on these features, offering a more robust and user-friendly tool for Meteor developers, with new features like integrations with popular services such as Slack and Datadog

# How to use it

First it's necessary to enter this command `meteor add montiapm:profiler montiapm:agent`

Rizomo Team currently uses [Monti APM](https://montiapm.com/) as the de-facto solution to manage and oversee the server performances , pubsub optimization, methods usage, bottlenecks, etc.

In order to ensure a fully controlled stack from end-to-end, it is possible to self-host Meteor-APM.

The agent meteor package by itself is opensource and compatible. Unless credentials are fed to it, it won't have any impact the app.

A Elastic based version of Meteor APM has also been forked and is available here : [https://forums.meteor.com/t/meteor-elastic-apm-with-meteor-metrics/49588](https://forums.meteor.com/t/meteor-elastic-apm-with-meteor-metrics/49588)

Meteor APM repo : [https://github.com/lmachens/meteor-apm-server](https://github.com/lmachens/meteor-apm-server)

Meteor Forums Discussion about self-hosting : [https://forums.meteor.com/t/self-hosted-apm-server/54483](https://forums.meteor.com/t/self-hosted-apm-server/54483)

# Screenshots

![Screen_Shot_2023-03-30_at_16.11.32](uploads/2a7d997fdad9f552eb67b5568b08e57a/Screen_Shot_2023-03-30_at_16.11.32.png)

![Screen_Shot_2023-03-30_at_16.11.13](uploads/049d55f600b0cc56e9ff26d1387ce872/Screen_Shot_2023-03-30_at_16.11.13.png)

![Screen_Shot_2023-03-30_at_16.11.00](uploads/a1106c952c5596f26b9c7a05e356aaa6/Screen_Shot_2023-03-30_at_16.11.00.png)

![Screen_Shot_2023-03-30_at_16.10.39](uploads/de48e3de170e404ae6465deb586b6063/Screen_Shot_2023-03-30_at_16.10.39.png)