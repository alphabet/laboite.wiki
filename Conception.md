Fonctionnement
==============

Les groupes
-----------

*  Un groupe peut être de type ouvert/modéré/privé/automatique
*  Un utilisateur peut avoir les rôles suivant sur un groupe: admin, animateur, membre, candidat
*  Si un groupe est ouvert, tout le monde peut se définir comme membre du groupe
*  Si un groupe est modéré, tout le monde peut se définir comme candidat au groupe. L'animateur ou l'admin peuvent valider les candidatures et ajouter des membres
*  Si un groupe est privé, seul l'admin et l'animateur peuvent ajouter des membres (pas de candidature).
*  Si un groupe est automatique (requête base ou annuaire) 

### Points à définir


* Est ce que tout le monde peut créer un groupe ? => OUI sauf groupe auto qui sont soumis à autorisation
* Est ce qu'un animateur de groupe peut ajouter/supprimer des membres ?
* Est ce que le champ owner d'un groupe est pertinent (le créateur est déjà mis comme admin du groupe) ?
* Peut on créer des utilisateurs locaux sans passer par keycloak (et si oui comment les associer ensuite) ?
* Quels sont les différents rôles ? Pour l'instant : admin, admin/group, animator/group, member/group, candidate/group.

## Les structures

* une structure permet de définir des services associés (application ou lien)
  * les services définis dans service de la structure apparaitront dans l'onglet "Ma Structure"
* lorsqu'un utilisateur s’enrôle dans l'application, actuellement, c'est lui qui choisi sa structure de rattachement.
  * un utilisateur peux changer de structure de rattachement sans validation
* il est possible d'avoir un (ou plusieurs) administrateur d'une structure afin de valider les utilisateurs de la structure


### Les structures multi-niveau