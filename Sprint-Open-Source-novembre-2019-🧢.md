## 19 novembre

### 🔐 Thématique de l'authentification 

#### Les contraintes 
- Pas d'e-mail unique (changements au cours de la carrière)
- Points de connexion multiples (nomadisme des agents)
- Restreindre le nombre d'entrées (trop de guichets affichés)
- Connexion / déconnexion facile (élèves et postes partagés)

### Thématique UI

- Choix de la bibliothèque de départ 
- Commencer à intégrer alpha
- Coder l'inscription 
