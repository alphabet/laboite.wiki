
| **Attention doc obsolète depuis l'arrivée du CI qui gère tout automatiquement** |
| ------------------------------------------------------------------------------- |

<details><summary>Voir quand même</summary>
Les branches de Hotfix ([référence](https://nvie.com/posts/a-successful-git-branching-model/#hotfix-branches)) sont destinées à corriger les bugs trouvés dans la branche de release (master), voir [Création d'une release (git)](Cr%C3%A9ation-d'une-release-(git)). Ce sont des branches temporaires dérivées de master et mergées ensuite à la fois dans master et dev. Cela générera donc une nouvelle release.

1. Définir le nouveau numéro de version de cette release avec correctif (NUM_VER = numéro de version voulue) :
```
NUM_VER="3.0.1"
echo "Numéro du hotfix : $NUM_VER"
```

2. Créer la branche de hotfix à partir de master :
```
git checkout -b hotfix-$NUM_VER master
```
3. Effectuer tous les changements relatifs à cette nouvelle version (package.json, changelog, aides...)

4. Commiter ces changements dans la branche de hotfix :
```
git commit -a -m "Update version number to $NUM_VER"
```
5. Corriger le bug et commiter :
```
git commit -m "Fixed severe production problem"
```
6. Merger cette branche de hotfix dans master :
```
git checkout master
git merge --no-ff hotfix-$NUM_VER
git push
```
7. Tagger la branche master :
```
git tag -a $NUM_VER -m "Hotfix $NUM_VER"
git push origin $NUM_VER
```
8. Répercuter les changements dans la branche dev :
```
git checkout dev
git merge --no-ff hotfix-$NUM_VER
git push
```
9. Supprimer la branche de hotfix :
```
git branch -d hotfix-$NUM_VER
```
</details>