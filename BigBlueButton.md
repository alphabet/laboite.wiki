**Première implémentation**

* Dans la page d'un groupe: si un utilisateur est membre ou animateur d'un groupe, un bouton lui permet de rejoindre le salon du groupe
* Quand quelqu'un joint le salon, une création du salon est systématiquement demandée avant de rejoindre (ignorée si il existe déjà).
* dans cette version, tout le monde peut créer le salon (animateur ou membre) et il n'y a pas d'options proposées lors de la création. La seule différence est que l'animateur est considéré comme un modérateur dans BigBlueButton.

pour tester, passer sur la branche groupMeetings et modifier settings.development.json :
```
{
  "public": {
    "enableBBB": true,
    "BBBUrl": "https://bbb-lms-scalelite.visio.education.fr/bigbluebutton/api"
  },
  "private: {
    "BBBSecret": "XXXXX" (voir titre de la tâche dans latelier)
  }
}
```

Il faut aller sur la page d'un groupe dont on est membre, un bouton devrait apparaitre dans les ressources du groupe.

**Idées d'évolutions**

* permettre la création du salon seulement par les animateurs et proposer des options ?
* afficher au niveau du groupe si une conférence est en cours pour le groupe (nécessite de faire des appels réguliers à l'API depuis la page du groupe, ou récupérer régulièrement la liste de toutes les conférences en cours côté serveur et publier dans une collection). pour les membres du groupe, on pourrait afficher le bouton seulement si une conférence est lancée (mais risque de surcharge de l'API si on interroge régulièrement), ou afficher un message d'erreur si connexion avant création de la conférence.

**quelques points à éclaircir**

* options par défaut des salons.
* j'ai essayé différentes valeurs pour les options d'enregistrement de la conférence, mais je n'ai jamais vu l'interface d'enregistrement en tant que modérateur (désactivé sur ce serveur ?)
* sécurité: 2 mots de passe (modérateur/utilisateur) sont générés à la création du salon. Il semble possible de les réutiliser chaque fois qu'on recrée le salon. Actuellement, on laisse BBB les regénèrer à chaque fois que le salon est recréé. Il remonte une erreur disant que le salon existe déjà quand un utilisateur essaye de le créer et que le salon existe (il ne le fait pas si on réutilise le même mot de passe). Je ne sais pas trop l'impact de sécurité si on veut conserver les mots de passe (sachant qu'on peut aussi fournir un attribut createTime à la connexion pour limiter les possibilités de rejouer l'URL).
* avec le fonctionnement actuel (mots de passes stockés dans la base côté serveur à chaque création) il y a probablement un risque que quelques connexions échouent si des utilisateurs essaient de se connecter pile au moment ou la conférence vient d'être créée (le temps que la base se mette à jour).

